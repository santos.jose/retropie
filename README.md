# Retropie in Raspberry with roms

## Description
Manual step by step to create a retropie with roms in Raspberry Pi.

## Installation
1. Download Raspberry PI IOS https://www.raspberrypi.com/software/
2. Download Retropie Image form https://retropie.org.uk/download/
3. Download roms from https://www.emuparadise.me/
4. Write your SD with your selected image.
5. Copy all your rom in the route /home/pi/RetroPie/roms/
6. Connected and enjoy your Retropie
